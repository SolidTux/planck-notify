use dbus::{
    arg::{self, RefArg, Variant},
    blocking::{stdintf::org_freedesktop_dbus::Properties, Connection},
    channel::MatchingReceiver,
    message::MatchRule,
};
use log::{debug, error, info};
use planck::{Command, Keyboard, KeyboardError};
use std::{collections::HashMap, error::Error, thread, time::Duration};
use stderrlog::Timestamp;

fn show_text(text: String) -> Result<(), KeyboardError> {
    info!("{}", text);
    let mut keyboard = Keyboard::open()?;
    keyboard.send(Command::RGBSave)?;
    for i in 0..3 {
        if i > 0 {
            keyboard.send(Command::Color(0, 0, 0))?;
            thread::sleep(Duration::from_millis(100));
        }
        keyboard.send(Command::Color(255, 0, 0))?;
        thread::sleep(Duration::from_millis(150));
    }
    keyboard.text(text, false, Duration::from_millis(100))?;
    keyboard.send(Command::Color(0, 0, 0))?;
    keyboard.send(Command::RGBRestore)?;
    Ok(())
}

fn get_progress(conn: &Connection, player: &str) -> Result<(f64, String), Box<dyn Error>> {
    let proxy_player = conn.with_proxy(
        player,
        "/org/mpris/MediaPlayer2",
        Duration::from_millis(5000),
    );
    let metadata: HashMap<String, Variant<Box<dyn RefArg>>> =
        proxy_player.get("org.mpris.MediaPlayer2.Player", "Metadata")?;
    let length: i64 = *arg::prop_cast(&metadata, "mpris:length").unwrap_or(&1);
    let position: i64 = proxy_player.get("org.mpris.MediaPlayer2.Player", "Position")?;
    let status: String = proxy_player.get("org.mpris.MediaPlayer2.Player", "PlaybackStatus")?;
    debug!("{}: {}/{} ({})", player, position, length, status);
    Ok(((position as f64) / (length as f64), status))
}

fn main() -> Result<(), Box<dyn Error>> {
    stderrlog::new()
        .module(module_path!())
        .verbosity(1)
        .timestamp(Timestamp::Second)
        .init()
        .unwrap();

    let conn = Connection::new_session()?;
    let mut notifications = MatchRule::new();
    notifications.path = Some("/org/freedesktop/Notifications".into());
    notifications.member = Some("Notify".into());

    let proxy = conn.with_proxy(
        "org.freedesktop.DBus",
        "/org/freedesktop/DBus",
        Duration::from_millis(5000),
    );
    proxy.method_call(
        "org.freedesktop.DBus.Monitoring",
        "BecomeMonitor",
        (vec![notifications.match_str()], 0u32),
    )?;
    conn.start_receive(
        notifications,
        Box::new(|msg, _| {
            let args: (
                Option<String>,
                Option<i64>,
                Option<String>,
                Option<String>,
                Option<String>,
            ) = msg.get5();
            let title = args.3.unwrap();
            if let Err(err) = show_text(title) {
                error!("{}", err);
            }
            true
        }),
    );

    let conn_mpris = Connection::new_session()?;
    let proxy_mpris = conn_mpris.with_proxy(
        "org.freedesktop.DBus",
        "/org/freedesktop/DBus",
        Duration::from_millis(5000),
    );
    loop {
        {
            let (names,): (Vec<String>,) =
                proxy_mpris.method_call("org.freedesktop.DBus", "ListNames", ())?;
            let players: Vec<String> = names
                .into_iter()
                .filter(|x| x.starts_with("org.mpris.MediaPlayer2"))
                .collect();
            match Keyboard::open() {
                Ok(mut keyboard) => {
                    let (_, w) = keyboard.canvas_size()?;
                    match players
                        .iter()
                        .find_map(|x| get_progress(&conn_mpris, x).ok())
                    {
                        Some((progress, status)) => {
                            if &status == "Paused" {
                                keyboard
                                    .send(Command::Progress(0, w, true, progress, 255, 255, 0))?;
                            } else {
                                keyboard
                                    .send(Command::Progress(0, w, true, progress, 0, 255, 0))?;
                            }
                        }
                        None => {
                            keyboard.send(Command::Progress(0, w, false, 0., 255, 255, 255))?;
                        }
                    }
                }
                Err(e) => error!("{}", e),
            }
        }
        conn.process(Duration::from_millis(1000)).unwrap();
    }
}
