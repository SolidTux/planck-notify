# planck-notify

A small daemon which listens to DBus notifications and displays them on a Planck keyboard.

# Installation

* Install [Rust](https://rustup.rs)
* Run `make install`
* Enable the service with `systemd --user enable --now planck-notify`

Currently the paths to the user systemd path and cargo home are hardcoded. If those do not work for you, adapt them in `Makefile` and `files/planck-notify.service` accordingly. The serial port device is also hardcoded to `/dev/ttyACM0` in the library part.
