SYSTEMD_DIR=${HOME}/.config/systemd/user

.PHONY: binary files

install: binary files

binary:
	cargo install --path . --force

files:
	install -D -t "${SYSTEMD_DIR}" files/planck-notify.service
